var indexRouter         = require('express').Router();
const usersRoute        = require('../routes/usersR');
const userSettRoute     = require('../routes/userSettingsR');
const chatRoute         = require('../routes/chatR');
const chatmessageRoute  = require('../routes/chatmessageR');
const transactRoute     = require('../routes/transactionR');
const mapRoute          = require('../routes/mapR');

/* GET home page. */
indexRouter.get('/', (req, res) => {
  res.status(200).json({ message: 'Welcome to My restful service' });
});

indexRouter.use('/user',usersRoute);
indexRouter.use('/usersettng',userSettRoute);
indexRouter.use('/chat',chatRoute);
indexRouter.use('/chatmessage',chatmessageRoute);
indexRouter.use('/transact',transactRoute);
indexRouter.use('/map',mapRoute);


module.exports = indexRouter;
