var transactRoute = require('express').Router();
const Transaction = require ('../models/transact');
const TranHist    = require ('../models/tranhist');
const TransactionHead    = require ('../models/tranheader');
const User = require ('../models/user');
Promise = require('bluebird')

transactRoute
.post('/addtran/:senderId',function(req, res) {
        var tranDetail = {};
        var senderId= req.params.senderId;
        tranDetail = req.body;	
        if(tranDetail.reqid==null||tranDetail.reqid==""||tranDetail.reqid=="undefined"){
            res.json({message:"Null request id sent"});
        }else{
            TransactionHead.
            findOne({$or:[
                    {trancredperson:tranDetail.trancredpersonID,trandebitperson:tranDetail.trandebitpersonID},
                    {trandebitperson:tranDetail.trancredpersonID,trancredperson:tranDetail.trandebitpersonID}
                     ]},function(err, tranHeadRecord){
              if (err)
                  res.json(err);
              if(tranHeadRecord!=""&&tranHeadRecord!=null){
                  console.log("Record Exists "+tranHeadRecord)
                  //do an update
                  tranHeadRecord.latestTrandate   = new Date().getTime();
                  tranHeadRecord.trandate         = new Date().getTime();
                  tranHeadRecord.tranAmt          = tranDetail.tranAmt;
                  tranHeadRecord.tranStatus       = tranDetail.tranStatus;
                  tranHeadRecord.tranperiod       = tranDetail.tranperiod;
                  tranHeadRecord.tipamount        = tranDetail.tipamount;
                  tranHeadRecord.lastUpdatedby        = senderId;
                  tranHeadRecord.hasPendingTransactn = true;
                  
                  tranHeadRecord.save(function(err) {
                        if (err)
                            res.send(err);
                        //Save tranbody
                        var transact = new Transaction();
                        transact.tranrequestid    = tranHeadRecord.tranrequestid;
                        transact.transactionid    = tranHeadRecord.tranrequestid+new Date().getTime();
                        transact.trandate         = new Date().getTime();
                        transact.latestTrandate   = new Date().getTime();
                        transact.tranperiod       = tranDetail.tranperiod;
                        transact.tranmethod       = tranDetail.tranmethod;
                        transact.tranRate         = tranDetail.tranRate;
                        transact.tipamount        = tranDetail.tipamount;
                        transact.tranAmt          = tranDetail.tranAmt;
                        transact.trandescription  = tranDetail.trandescription;
                        transact.trancredperson   = tranDetail.trancredpersonID;
                        transact.trandebitperson  = tranDetail.trandebitpersonID;
                        transact.tranStatus       = tranDetail.tranStatus;

                        transact.save(function(err) {
                            if (err)
                                res.send(err);
                            res.json({ message: 'Transaction Updated ',tranrequestId:transact.tranrequestid,data:transact});
                        });
                    });
              }else{
                var transactionHead = new TransactionHead();
                transactionHead.tranrequestid    = tranDetail.reqid;
                transactionHead.trandate         = new Date().getTime();
                transactionHead.latestTrandate   = new Date().getTime();
                transactionHead.tranperiod       = tranDetail.tranperiod;
                transactionHead.tranmethod       = tranDetail.tranmethod;
                transactionHead.tranRate         = tranDetail.tranRate;
                transactionHead.tipamount        = tranDetail.tipamount;
                transactionHead.tranAmt          = tranDetail.tranAmt;
                transactionHead.trandescription  = tranDetail.trandescription;
                transactionHead.trancredperson   = tranDetail.trancredpersonID;
                transactionHead.trandebitperson  = tranDetail.trandebitpersonID;
                transactionHead.tranStatus       = tranDetail.tranStatus;
                transactionHead.lastUpdatedby        = senderId;
                transactionHead.hasPendingTransactn = true;

                transactionHead.save(function(err) {
                    if (err)
                        res.send(err);
                    //Save tranbody
                    var transact = new Transaction();
                    transact.tranrequestid    = tranDetail.reqid;
                    transact.transactionid    = tranDetail.reqid+new Date().getTime();
                    transact.trandate         = new Date().getTime();
                    transact.latestTrandate   = new Date().getTime();
                    transact.tranperiod       = tranDetail.tranperiod;
                    transact.tranmethod       = tranDetail.tranmethod;
                    transact.tranRate         = tranDetail.tranRate;
                    transact.tipamount        = tranDetail.tipamount;
                    transact.tranAmt          = tranDetail.tranAmt;
                    transact.trandescription  = tranDetail.trandescription;
                    transact.trancredperson   = tranDetail.trancredpersonID;
                    transact.trandebitperson  = tranDetail.trandebitpersonID;
                    transact.tranStatus       = tranDetail.tranStatus;

                    transact.save(function(err) {
                        if (err)
                            res.send(err);
                        res.json({ message: 'Transaction Initiated',tranrequestId:transact.tranrequestid,data:transact});
                    });  
                });
              }
          });
        }
        
})

.put('/update',function(req, res) {
    var tranrequestid = req.query.tranrequestid;
    var lastUpdateby = req.query.senderId;
    console.log("Updating tran read for: "+tranrequestid+" and "+lastUpdateby);
    TransactionHead.findOne({tranrequestid:tranrequestid,hasPendingTransactn:true,lastUpdatedby:{ $ne: lastUpdateby }},function(err,tranHeaddet) {
        if(err)
            res.send(err);
        if(tranHeaddet==null||tranHeaddet==null){
            res.json({ message: 'transaction not found!',respCode:"96"});
        }else{
            tranHeaddet.hasPendingTransactn = false;
            tranHeaddet.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'transaction read!',respCode:"00"});
            });
        }
    });
})
// .post('/addstatus',function(req, res) {
//     var tranDetail = {};
//     tranDetail = req.body;	
//     if(tranDetail.tranrequestid==null||tranDetail.tranrequestid==""||tranDetail.tranrequestid=="undefined"){
//         res.json({message:"Invalid Request ID Sent"});
//     }else{
//         TranHist.find({tranrequestid:tranDetail.tranrequestid},function(err, tranRecord) {
//           if (err)
//               res.json(err);
//           if(tranRecord==""){
//               res.json({ message: 'Transaction ID is Invalid' });
//           }else{
//             var alreadyExist=false;
//             for(let i = 0; i < tranRecord.length; i++){
//                 if(tranRecord[i].tranStatus==tranDetail.tranStatus){
//                     alreadyExist = true;
//                 }
//             }
//             if(alreadyExist){
//                 res.json({ message: 'Status Already Exist'});
//             }else{
//                 var tranhist = new TranHist();
//                 tranhist.tranrequestid    = tranDetail.tranrequestid;
//                 tranhist.latestTrandate   = new Date().getTime();
//                 tranhist.tranStatus       = 2;
//                 tranhist.statusDescriptn  = tranDetail.tranStatus;
//                 tranhist.save(function(err) {
//                     if (err){
//                         console.log("issh: "+err)
//                         res.send(err);
//                     }else{
//                         //Update transaction last status
//                         Transaction.findOne({tranrequestid:tranDetail.tranrequestid},function(err,tran){
//                             if(err){
//                                 console.log("issh: "+err)
//                                 res.send(err);
//                             }else{
//                                 tran.tranStatus = tranDetail.tranStatus;
//                                 tran.save(function(err){
//                                     if (err){
//                                         console.log("issh: "+err)
//                                         res.send(err);
//                                     }else{
//                                         res.json({ message: 'Tran Status Added!',data:tranhist});
//                                     }
//                                 });
//                             }
                            
//                         })
//                     }
                    
                    
//                 });
//             }
//           }
//         });
//     }
    
// })

.post('/updateTran/:senderId',function(req, res) {
    var tranDetail = {};
    var senderId= req.params.senderId;
    tranDetail = req.body;	
    if(tranDetail.transactionid==null||tranDetail.transactionid==""||tranDetail.transactionid=="undefined"){
        res.json({message:"Invalid Request ID Sent"});
    }else{
        Transaction.findOne({transactionid:tranDetail.transactionid},function(err, tranRecord) {
          if (err)
              res.json(err);
          if(tranRecord==""){
              res.json({ message: 'Transaction ID is Invalid' });
          }else{
            tranRecord.tranStatus       = tranDetail.tranStatus
            tranRecord.latestTrandate   = new Date().getTime();
                tranRecord.save(function(err) {
                    if (err){
                        console.log("issh: "+err)
                        res.send(err);
                    }else{
                        TransactionHead.findOne({tranrequestid:tranRecord.tranrequestid},function(err, tranHead) {
                            if (err)
                                res.json(err);
                            if(tranHead==""){
                                res.json({ message: 'Transaction ID is Invalid' });
                            }else{
                                tranHead.tranStatus       = tranDetail.tranStatus
                                tranHead.lastUpdatedby    = senderId;
                                tranHead.hasPendingTransactn = true;
                                tranHead.latestTrandate   = new Date().getTime();
                                    tranHead.save(function(err) {
                                        if (err)
                                            res.json(err);
                                        res.json({ message: 'Transaction Updated',tranrequestId:tranRecord.tranrequestid,data:tranHead });
                                    })
                            }
                        })
                    }
            
                })
            }
        })
    
}})

.get('/fetchtranhead',function(req, res) {
    console.log("Querying Transaction Table using senderID: "+req.query.senderID);
    var searhKey;
    var tranList=[];
    var senderID    = req.query.senderID;
    var lstMsgDate  = req.query.msgDate;
    if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
    }

    var tranCount;
    var newTrans=0;
    
    TransactionHead.count({$or:[{trancredperson:senderID},{trandebitperson:senderID}],latestTrandate:{ $gte: lstMsgDate }},function(err,transCount){
        if(err)
            res.json({ respcode:"96",status:"failed",message: err });
        tranCount = transCount;

        TransactionHead.find({$or:[{trancredperson:senderID},{trandebitperson:senderID}],latestTrandate:{ $gte: lstMsgDate }})
        .then(function(tranHeads){
                var tranItems=[];
                tranHeads.forEach(function(u) {
                  searhKey = u.trancredperson==senderID?u.trandebitperson:u.trancredperson;
                  console.log("Checking with: "+senderID+" and "+u.lastUpdatedby)
                  if(senderID!=u.lastUpdatedby){
                      console.log("got in here with: "+u.hasPendingTransactn)
                      if(u.hasPendingTransactn){
                        newTrans = newTrans +1;
                      }
                      console.log("val now: "+newTrans)
                  }
                  
                  tranItems.push(
                    User.findOne({uid:searhKey}, function(err, user) {
                      if (err){
                        res.send(err);
                      }	
                      var cellCont = {
                          tranHead:{
                            tranrequestid:u.tranrequestid,
                            recipientName:user.username,
                            recipientImage:"img",
                            recipientPhnNumber:user.phoneNumber,
                            recipientID:user.uid
                          },
                          tranBody:u
    
                      }
                      tranList.push(cellCont);
                    })
                  );
                });
                return Promise.all(tranItems);
        })
        .then(resp=>{
            res.json({metadata:{tranCount:tranCount,newTrans:newTrans},respcode:"00",message:"Inquiry succesfully",data:tranList});
        }).catch(err=>{
            var fatErr = "Reason for failure is: "+err;
            res.json({ message: fatErr,responseCode:"96",data:[] });
        });
    });

    
})

.get('/fetchtran/',function(req, res) {
    console.log("Querying Transaction Table using tranrequestid: "+req.query.tranrequestid);
    var searhKey;
    var tranList=[];
    var reqID    = req.query.tranrequestid;
    var lstMsgDate  = req.query.msgDate;
    if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
    }
    Transaction.find({tranrequestid:reqID,latestTrandate:{ $gte: lstMsgDate }})
    .then(function(tranDetails){
            var tranItems=[];
            tranDetails.forEach(function(u) {
              tranItems.push(
                User.findOne({uid:u.trancredperson}, function(err, user) {
                  if (err){
                    res.send(err);
                  }	
                  u.tranTitle = user.username;
                  tranList.push(u);
                })
              );
            });
            return Promise.all(tranItems);
        })
        .then(resp=>{
            res.json(tranList);
        }).catch(err=>{
            var fatErr = "Reason for failure is: "+err;
            res.json({ message: fatErr });
        });
})


.get('/fetchalltran/',function(req, res) {
    console.log("Querying Transaction Table using tranrequestid: "+req.query.userID);
    var searhKey;
    var tranList=[];
    var userID    = req.query.userID;
    var lstMsgDate  = req.query.msgDate;
    if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
    }
    Transaction.find({$or:[{trandebitperson:userID},{trancredperson:userID}],latestTrandate:{ $gte: lstMsgDate }})
    .then(function(tranDetails){
            var tranItems=[];
            tranDetails.forEach(function(u) {
              tranItems.push(
                User.findOne({uid:u.trancredperson}, function(err, user) {
                  if (err){
                    res.send(err);
                  }	
                  u.tranTitle = user.username;
                  tranList.push(u);
                })
              );
            });
            return Promise.all(tranItems);
        })
        .then(resp=>{
            res.json(tranList);
        }).catch(err=>{
            var fatErr = "Reason for failure is: "+err;
            res.json({ message: fatErr });
        });
})

// .get('/fetchhist/:tranreqID',function(req, res) {
//     var tranID = req.params.tranreqID;
//     TranHist.find({tranrequestid:tranID}).sort([['latestTrandate', 1]]).exec(function(err, statList) {
//         if (err)
//             res.send(err);
//         res.json(statList);
//     });
// })

.get('/all',function(req, res) {
  Transaction.find().sort([['latestTrandate', 1]]).exec(function(err, tranList) {
      if (err)
          res.send(err);

      res.json(tranList);
  });
})

.get('/delete/all',function(req, res) {
  Transaction.remove(function(err, tranList) {
      if (err)
          res.send(err);
      res.json(tranList);
  });
})

.get('/deletehist/all',function(req, res) {
    TranHist.remove(function(err, tranList) {
        if (err)
            res.send(err);
        res.json(tranList);
    });
})

.get('/deletehead/all',function(req, res) {
    TransactionHead.remove(function(err, tranHead) {
        if (err)
            res.send(err);
        res.json(tranHead);
    });
})

.put('/',function(req, res) {
    Transaction.findOne({tranrequestid:req.params.tranID}, function(err, trans) {

        if (err)
            res.send(err);

        trans.tranStatus = req.body.tranStatus;  // update the bears info
        trans.latestTrandate = new Date().getTime();
        // save the transaction
        trans.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Transaction updated!',data: trans});
        });

    });
})

module.exports = transactRoute;
