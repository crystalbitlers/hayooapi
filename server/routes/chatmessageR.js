var chatmessageRoute = require('express').Router();
const ChatMessageModel = require ('../models/chatmessages');
const ChatModel = require ('../models/chat');
Promise = require('bluebird')

chatmessageRoute
.post('/add',function(req, res) {
  var msgDetail = {};
  msgDetail = req.body;
  var chatMessage = new ChatMessageModel();
  var msgid       = msgDetail.msgID;
  console.log("ID is: "+msgid);
  chatMessage.msgID         =  msgid;	
  chatMessage.senderuid     = msgDetail.senderuid;
  chatMessage.recieveruid   = msgDetail.recieveruid;
  chatMessage.msgfrom       = msgDetail.from;
  chatMessage.msgto         = msgDetail.to;
  chatMessage.msg           = msgDetail.body;
  chatMessage.msgtype       = msgDetail.type;
  chatMessage.datetime      = new Date().getTime();
  chatMessage.msgstatus     = 0;
  chatMessage.isRead        = false;
  chatMessage.save(function(err) {
    if (err){
        res.send(err);
    }else{
        console.log("Message Inserted");
        res.json({ message: 'Message Inserted!' });
    }
      
  });		
})
.get('/fetch',function(req, res) {
    var senderID = req.query.senderuid;
    var recvID = req.query.recieveruid;
    var lstMsgDate = req.query.msgDate;
    console.log("Fetching Using: senderID "+senderID+" and recieverID "+recvID+" and msgDate "+lstMsgDate);
    if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
    }
    ChatMessageModel.find({senderuid:{$in:[senderID,recvID]}
                        ,recieveruid:{$in:[senderID,recvID]}
                        ,datetime:{$gte:lstMsgDate}}, function(err, chatmessage) {
        if (err)
            res.send(err);
        res.json(chatmessage);
    });
})
.get('/:searchkey',function(req, res) {
    var searchkey = req.params.searchkey;
        ChatMessageModel.find({$or:[{senderuid:searchkey},{recieveruid:searchkey}]}, function(err, chatmessage) {
            if (err)
                res.send(err);
            res.json(chatmessage);
        });
})
.put('/update',function(req, res) {
    var chatId = req.query.chatId;
    var lastSender = req.query.senderId;
    console.log("Updating chat read for: "+chatId+" and "+lastSender);
    ChatModel.findOne({chatID:chatId,hasPendingMssg:true,senderuid:{ $ne: lastSender } },function(err,chatRecord) {
        if(err)
            res.send(err);
        console.log("chat  found "+chatRecord);
        if(chatRecord==null||chatRecord==''){
            console.log("chat not found! "+chatId+" and "+lastSender);
            res.json({ message: 'chat not found!',respCode:"96"});
        }else{
            chatRecord.hasPendingMssg = false;
            chatRecord.save(function(err) {
                if (err)
                    {res.send(err);}
                ChatMessageModel.update({msgID:chatRecord.chatID},{isRead:true},function(err,chatmessages){
                    if (err)
                        res.send(err);
                    res.json({ message: 'chat read!',respCode:"00"});
                })
                
            });
        }
    });

})
.get('/fetch/all',function(req, res) {
	ChatMessageModel.find(function(err, chatMsgList) {
		if (err)
			res.send(err);
		res.json(chatMsgList);
	});
})
.get('/delete/all',function(req, res) {
    ChatMessageModel.remove(function(err, chatMsgList) {
        if (err)
            res.send(err);
        res.json(chatMsgList);
    });
});

module.exports = chatmessageRoute;
