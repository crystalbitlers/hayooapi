var chatRoute = require('express').Router();
const ChatModel = require ('../models/chat');
const User = require ('../models/user');
Promise = require('bluebird')

chatRoute
.post('/add',function(req, res) {
	var chatmodel;
	var chatDetail = {};
	chatDetail = req.body;
	var chatid = chatDetail.chatID;
	console.log("Querying Table with "+chatid);
	ChatModel.findOne({$or:[{senderuid:chatDetail.senderuid,recieveruid:chatDetail.recieveruid},
							{senderuid:chatDetail.recieveruid,recieveruid:chatDetail.senderuid}
							]}, function(err, chatDetails) {
            if (err)
                res.send({respcode:"96",message:"Chat creation failed",data:err});
			if(chatDetails!=null){
				chatDetails.lastMsgText 	= chatDetail.lastMsgText;
				chatDetails.lastMsgDate 	= new Date().getTime();
				chatDetails.senderuid    	= chatDetail.senderuid;
				chatDetails.recieveruid   	= chatDetail.recieveruid;
				chatDetails.chatStatus 		= 0;
				chatDetails.hasPendingMssg	= true;
				chatDetails.save(function(err) {
					if (err)
						res.send({respcode:"96",message:"Chat creation failed",data:err});
					console.log("Chat Updated: ");
					res.json({ respcode:"00",message: 'Chat Updated!',data:ChatModel,metadata:{msg:"nothing special yet"} });
				});
			}else{
				chatmodel = new ChatModel();
				chatmodel.chatID 		= chatid;
				chatmodel.chatTitle 	= chatDetail.chatTitle;
				chatmodel.senderuid    	= chatDetail.senderuid;
				chatmodel.recieveruid   = chatDetail.recieveruid;
				chatmodel.datetime  	= new Date().getTime();
				chatmodel.lastMsgText 	= chatDetail.lastMsgText;
				chatmodel.lastMsgDate 	= new Date().getTime();
				chatmodel.recieverName  = chatDetail.recieverName;
				chatmodel.chatStatus	= 0;
				chatmodel.hasPendingMssg= true;

				chatmodel.save(function(err) {
					if (err)
						res.send({respcode:"96",message:"Chat creation failed",data:err});
					res.json({ respcode:"00",message: 'Chat Inserted!',data:ChatModel,metadata:{msg:"nothing special yet"} });
				});
			}
            
	});
})
.get('/fetch',function(req, res) {
	console.log("Querying Chat Table using senderID: "+req.query.senderID);
	var searhKey;
	var chatList=[];
	var senderID 	= req.query.senderID;
	var lstMsgDate 	= req.query.msgDate;
	if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
	}
	var mssgCount;
	var unreadChats=0;
	
	ChatModel.count({$or:[{senderuid:senderID},{recieveruid:senderID}],lastMsgDate:{ $gte: lstMsgDate },chatStatus:0},function(err,chatCount){
		if(err)
			res.json({ respcode:"96",status:"failed",message: fatErr });
		if(chatCount!=""){
			mssgCount = chatCount;
			ChatModel.find({$or:[{senderuid:senderID},{recieveruid:senderID}],lastMsgDate:{ $gte: lstMsgDate }})
			.then(function(chatDetails) {
				var chatItems = [];
				chatDetails.forEach(function(u) {
					if(u.senderuid==senderID){
						searhKey = u.recieveruid
					}else{
						searhKey = u.senderuid
						unreadChats = u.hasPendingMssg==true?++unreadChats:unreadChats;
					}
						chatItems.push(
							User.findOne({uid:searhKey}, function(err, user) {
							if (err){
								res.send({respcode:"96",message:"Chat Inquiry failed",data:err});
							}
							u.recieverName = user.username;
							chatList.push(u);
						}));
				});
				return Promise.all(chatItems);
			})
			.then(resp=> {
				console.log(JSON.stringify(chatList))
				res.json({metadata:{chatCount:chatCount,unreadChats:unreadChats},respcode:"00",message:"Inquiry succesfully",data:chatList});
			}).catch(err=> {
				var fatErr="Reason for failure is: "+err;
				console.log(fatErr);
				res.json({ respcode:"96",status:"Inquiry Failed",message: fatErr });
			});
		}
		
	})
	
})

.get('/all',function(req, res) {
	ChatModel.find(function(err, chatList) {
		if (err)
			res.send({respcode:"96",message:"Chat Inquiry failed",data:err});
  
		res.json(chatList);
	});
  })
.get('/delete/all',function(req, res) {
	ChatModel.remove(function(err, chatList) {
		if (err)
			res.send({respcode:"96",message:"Chat Delete failed",data:err});
		res.json(chatList);
	});
});
//This would be used for chat settings like changing title,image e.t.c
// .put('/',function(req, res) {
// 		var chatDetail = {};
// 		chatDetail = req.body;
//         ChatModel.find({senderuid:chatDetail.senderuid,recieveruid:chatDetail.recieveruid}, function(err, chatmodel) {
//             if (err)
//                 res.send({respcode:"96",message:"Chat creation failed",data:err});
//             chatmodel.lastMsgText = chatDetail.lastMsgText;
// 			      chatmodel.lastMsgDate = chatDetail.lastMsgDate;
//             chatmodel.save(function(err) {
//                 if (err)
//                     res.send({respcode:"96",message:"Chat creation failed",data:err});
// 				        console.log("Chat Updated: ");
//                 res.json({ message: 'chat updated!' });
//             });

//         });
//  })
    
module.exports = chatRoute;
