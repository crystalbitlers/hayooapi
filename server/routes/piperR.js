var userRoute = require('express').Router();
const User = require ('../models/user');
const UserSettings = require ('../models/userSett');
var defSettings = {
    "contactisSeenBy":"all",
    "notifyBy":"mail",
    "serviceCategory":"computing",
    "description": "",
    "fee":54,
    "isNotified": false,
    "isClient":false,
    "seentutorial": true,
    "reqKeyforTran": false,
    "isCashEnabled": true,
    "isWalletEnabled":false,
    "isCardEnabled": false,
    "presntlocation": {"name":"simpson","lat":6.3243,"lng":3.1231,"center":"Post Office"}  
}

userRoute
      .get('/',function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);

            res.json(users);
        });
      })
      .post('/signin',function(req, res) {
            var signInDet = {};
			signInDet = req.body;
			console.log(JSON.stringify(signInDet));	
			console.log("Searching with "+signInDet.email);
			User.find({email:signInDet.email,passkey:signInDet.password},function(err, user) {
				if (err)
					res.json(err);
				console.log("Found "+user);
				if(user!=""){
					res.json({ message: 'user exists',data:user});
				}else{
					console.log("not a user");
					res.json({ message: 'not a user' });
				}
			});

      })
      .post('/signup',function(req,res){
        var user = new User();      // create a new instance of the user model
        console.log("incoming")
        var signUpDet = {};
        signUpDet = req.body;	
        user.email    = signUpDet.email;
        var idGenerated = "hy"+signUpDet.phonenumber;
        console.log("Searching with "+user.email);
        User.find({$or:[{email:user.email},{uid:idGenerated}]},function(err, users) {
            if (err)
                res.json(err);
            console.log("Found "+users);//else if no error
            if(users!=""){
                res.json({ message: 'Already exists' });
            }else{
                user.uid        = idGenerated;
                user.username   = signUpDet.firstname;
                user.phoneNumber= signUpDet.phonenumber;
                user.passkey  	= signUpDet.password;
                user.firstname	= signUpDet.firstname;
                user.lastname 	= signUpDet.lastname;
                user.birthdate	= signUpDet.birthdate;
                user.address	= signUpDet.address;
                user.email	 	= signUpDet.email;
                user.photo		= signUpDet.photo;
                user.isActive   = true;
                user.save(function(err) {
                    if (err)
                        res.send(err);
                    console.log("user created wihh data "+user);
                    var userSettings = new UserSettings(defSettings);
                    userSettings.uid = user.uid;
                    userSettings.save(function(err) {
                        if (err)
                            res.send(err);
                        console.log("userSettings created wihh data "+userSettings);
                        res.json({ message: 'user created!',data:user});
                    });
                });
            }
        });
      })
      .get('/:userid',function(req, res) {
        console.log("fetching with: "+req.params.userid)
            User.find({uid:req.params.userid}, function(err, user) {
                if (err)
                    res.send(err);
          console.log("Found User: "+user)
                res.json(user);
            });
        })
      .put('/:userid',function(req, res) {
            var updData = req.body;
            //work on updating based on keys in json sent: to reduce traffic content
            // use our user model to find the user we want
            User.findOne({uid:req.params.userid}, function(err, user) {
                if (err)
                    res.send(err);
            if(updData.username!=null&&updData.username!=""){
                user.username 		= updData.username;
            }
            user.phoneNumber  	= updData.phonenumber;
            user.passkey  		= updData.password;
            user.firstname		= updData.firstname;
            user.lastname 		= updData.lastname;
            user.birthdate	 	= updData.birthdate;
            user.address	 	= updData.address;
            user.email	 		= updData.email;
            user.isAgent		= updData.isAgent;
            user.photo			= updData.photo;
          
                // update the user
                user.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'user updated!' });
                });

            });
        })
    .put('/delete/:userid',function(req, res) {
        User.findOne({uid:req.params.userid}, function(err, user) {
            if (err)
                res.send(err);      
            // update the user
            user.isActive = false;
            user.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'user deleetd!' });
            });

        });
    });
    
    
module.exports = userRoute;
