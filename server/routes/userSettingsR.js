var userSettRoute = require('express').Router();
const UserSett = require ('../models/userSett');

userSettRoute
    .get('/all',function(req, res) {
		console.log("fetching with: "+req.params.userid)
        UserSett.find(function(err, userSett) {
            if (err)
                res.send(err);
			console.log("Found Usersettings: "+userSett)
            res.json(userSett);
        });
    })
    .get('/:userid',function(req, res) {
		console.log("fetching with: "+req.params.userid)
        UserSett.find({uid:req.params.userid}, function(err, userSett) {
            if (err)
                res.send(err);
			console.log("Found User: "+userSett)
            res.json(userSett);
        });
    })
	.put('/:userid',function(req, res) {
		var updData = req.body;
        // use our user model to find the user we want
        UserSett.findOne({uid:req.params.userid}, function(err, userSett) {
            if (err)
                res.send(err);
            console.log("Found Settings Record: "+userSett);
            if(userSett!=null&&userSett!=""){
                userSett.contactisSeenBy= updData.contactisSeenBy;
                userSett.addressisSeenBy= updData.addressisSeenBy;
                userSett.notifyBy  	    = updData.notifyBy;
                userSett.serviceCategory= updData.serviceCategory;
                userSett.description	= updData.description;
                userSett.fee 		    = updData.fee;
                userSett.isNotified	    = updData.isNotified;
                userSett.isClient	 	= updData.isClient;
                userSett.seentutorial	= updData.seentutorial;
                userSett.reqKeyforTran	= updData.reqKeyforTran;
                userSett.isCashEnabled	= updData.isCashEnabled;
                userSett.isWalletEnabled= updData.isWalletEnabled;
                userSett.isCardEnabled	= updData.isCardEnabled;
                userSett.presntlocation	= updData.presntlocation;
                userSett.isActive	= updData.isActive;
                
                //Update user settings
                userSett.save(function(err) {
                    if (err)
                        res.send(err);
                    console.log("UserSetings updated!");
                    res.json({ message: 'UserSetings updated!' });
                });

            }else{
                console.log("No Record Found");
                res.json({ message: 'No Record Found' });
            }
            

        });
    })
	.put('/delete/:userid',function(req, res) {
        UserSett.remove({uid:req.params.userid},function(err,userSett){
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

    
module.exports = userSettRoute;
