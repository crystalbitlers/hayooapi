var userRoute = require('express').Router();
const User = require ('../models/user');
const UserSettings = require ('../models/userSett');
Promise = require('bluebird')

var defSettings = {
    "contactisSeenBy":"all",
    "notifyBy":"P",
    "serviceCategory":"computing",
    "description": "",
    "fee":50,
    "isNotified": false,
    "isClient":false,
    "seentutorial": true,
    "reqKeyforTran": false,
    "isCashEnabled": true,
    "isWalletEnabled":false,
    "isCardEnabled": false,
    "presntlocation": {"name":"simpson","lat":6.3243,"lng":3.1231,"center":"Post Office"}  
}

userRoute
.get('/',function(req, res) {
    User.find(function(err, users) {
        if (err)
            res.send(err);

        res.json(users);
    });
})
.post('/signin',function(req, res) {
    var signInDet = {};
    signInDet = req.body;
    console.log("Searching with "+signInDet.email);
    User.find({email:signInDet.email,passkey:signInDet.password},function(err, user) {
        if (err)
            res.json(err);
        if(user!=""){
            res.json({ message: 'user exists',data:user});
        }else{
            res.json({ message: 'not a user' });
        }
    });

})
.post('/signup',function(req,res){
    var user = new User();      // create a new instance of the user model
    var signUpDet =req.body;	
    user.email    = signUpDet.email;
    var idGenerated = "hy"+signUpDet.phoneNumber;
    console.log("Searching with "+user.email);
    User.find({$or:[{email:user.email},{uid:idGenerated}]},function(err, users) {
        if (err)
            res.json(err);
        if(users!=""){
            res.json({ message: 'Already exists' });
        }else{
            user.uid        = idGenerated;
            user.username   = signUpDet.firstname;
            user.phoneNumber= signUpDet.phoneNumber;
            user.passkey  	= signUpDet.password;
            user.firstname	= signUpDet.firstname;
            user.lastname 	= signUpDet.lastname;
            user.birthdate	= signUpDet.birthdate;
            user.address	= signUpDet.city;
            user.email	 	= signUpDet.email;
            user.photo		= signUpDet.photo;
            user.isActive   = true;
            user.userType    = signUpDet.userType;
            user.save(function(err) {
                if (err)
                    res.send(err);
                var userSettings = new UserSettings(defSettings);
                userSettings.uid = user.uid;
                userSettings.save(function(err) {
                    if (err)
                        res.send(err);
                    res.json({ message: 'user created!',data:user});
                });
            });
        }
    });
})
.get('/:userid',function(req, res) {
    console.log("fetching with: "+req.params.userid)
    User.find({uid:req.params.userid}, function(err, user) {
        if (err)
            res.send(err);
    console.log("Found User: "+user)
        res.json(user);
    });
})
.put('/:userid',function(req, res) {
    var updData = req.body;
    //work on updating based on keys in json sent: to reduce traffic content
    // use our user model to find the user we want
    User.findOne({uid:req.params.userid}, function(err, user) {
        if (err)
            res.send(err);
    if(updData.username!=null&&updData.username!=""){
        user.username 		= updData.username;
    }
    user.phoneNumber  	= updData.phoneNumber;
    user.passkey  		= updData.passkey;
    user.firstname		= updData.firstname;
    user.lastname 		= updData.lastname;
    user.birthdate	 	= updData.birthdate;
    user.address	 	= updData.address;
    user.email	 		= updData.email;
    user.userType		= updData.userType;
    user.photo			= updData.photo;
    if(updData.presntlocation!=""&&updData.presntlocation!=null){
        user.presntlocation = updData.presntlocation;
    }   

        user.save(function(err) {
            if (err)
                res.send(err);
            // if(user.userType){
            //     //save category type
            // }
            res.json({ message: 'user updated!' });
        });

    });
})
.put('/delete/:userid',function(req, res) {
    User.findOne({uid:req.params.userid}, function(err, user) {
        if (err)
            res.send(err);      
        // update the user
        user.isActive = false;
        user.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'user deleetd!' });
        });

    });
})
.get('/delete/all',function(req, res) {
    User.remove(function(err, user) {
            if (err)
                res.send(err);
        console.log("Deleted User: "+user)
        res.json(user);
    });
})
.get('/filter/all/',function(req, res) {
    console.log("fetching : "+req.query.uid+" and "+req.query.service)
    User.find({uid:{$ne:req.query.uid}}, function(err, user) {
        if (err)
            res.send(err);
    console.log("Found User: "+user)
        res.json(user);
    });
})
    
    
module.exports = userRoute;
