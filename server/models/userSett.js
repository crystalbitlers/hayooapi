var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSettngsSchema   = new Schema({
    uid: String,
    contactisSeenBy:String,
    addressisSeenBy:String,
    notifyBy: String,
    serviceCategory:String,
    description:String,
    fee:Number,
    isNotified: false,
    isClient: false,
    isAgent:  false,
    seentutorial:false,
    reqKeyforTran:false,
    reqKeyforSettings:false,
    isCashEnabled:false,
    isWalletEnabled:false,
    isCardEnabled:false,
    isActive:false,
    isDeleted:false,
    regLocation:{name:String,lat:Number,lng:Number,center:String}
});

module.exports = mongoose.model('UserSett', UserSettngsSchema);