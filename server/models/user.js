var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
  id: Number,
  uid: String,
  username: String,
  firstname:String,
  lastname:String,
  birthdate: Number,
  email: String,
  passkey: String,
  phoneNumber:String,
  address:String,
  photo: String,
  userType:String,
  presntlocation:{name:String,lat:Number,lng:Number,center:String},
  isActive:false
});

module.exports = mongoose.model('User', UserSchema);