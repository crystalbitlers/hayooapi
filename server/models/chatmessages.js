var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ChatMessageSchema   = new Schema({
  msgID: String,
  senderuid: String,
  recieveruid: String,
  msgfrom:String,
  msgto:String,
  msg: String,
  msgtype:String,
  datetime: Number,
  mssgStatus:Number,
  isRead:false
});

module.exports = mongoose.model('ChatMessageModel', ChatMessageSchema);