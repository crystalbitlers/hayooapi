var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TranHistSchema   = new Schema({
  tranrequestid: String,
  latestTrandate: Number,
  tranStatus:Number,
  statusDescriptn:String
});

module.exports = mongoose.model('TranHist', TranHistSchema);