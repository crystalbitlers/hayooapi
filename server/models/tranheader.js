var Mongoose     = require('mongoose');
var Schema       = Mongoose.Schema;

var TransationHeadSchema   = new Schema({
  tranrequestid: String,
  tranperiod:Number,
  tranmethod:String,
  tranRate:Number,
  tipamount:Number,
  tranAmt: Number,
  tranStatus: String,
  trandescription: String,
  trandate: Number,
  latestTrandate: Number,
  trancredperson:String,
  trandebitperson: String
  ,tranTitle:String,
  hasPendingTransactn:false,
  lastUpdatedby:String
});

module.exports = Mongoose.model('TransactionHead', TransationHeadSchema);