var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var mapSchema = new Schema({
	loc: {
		type: {
			type: String,default: "Point"
		},
		coordinates: {
			type: [Number]
		}
	}
});  

mapSchema.index({ loc: '2dsphere'});

module.exports = mongoose.model('MapModel', mapSchema);