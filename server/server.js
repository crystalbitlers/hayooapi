var exp = require('express');
var app         = require('express')();
var mongoose    = require('mongoose');
mongoose.Promise = require('bluebird');
var logger      = require('morgan');
var bodyParser  = require('body-parser');
var cors        = require('cors');
const router 	= require('./routes');
var db;
// const ur1 = 'mongodb://127.0.0.1:27017/markers';
const ur1 = 'mongodb://admin2:admin1234@ds163826.mlab.com:63826/markers';
app.use(exp.static('hayoo/'));


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); 
app.use(logger('dev')); 
app.use(cors());


mongoose.Promise = global.Promise;
mongoose.connect(ur1,function (err, database){
	if (err) {
		console.log(err);
		console.log("Database connection failed "+err);
		process.exit(1);
	}
	db = database;
	console.log("Database connection to "+ur1+" ready");
	 var server = app.listen(process.env.PORT || 8087, function () {
		var port = server.address().port;
		console.log("App now running on port", port);
	  });
});

// router.use(function(req, res, next) {
//     // do logging
//     console.log('Something about to  Happen.');
//     next(); // make sure we go to the next routes and don't stop here
// });

app.get('/', (req, res) => {
	res.status(200).json({ message: 'Welcome to PayPorter API' });
});

app.get('/testTime', (req, res) => {
	var currTime = new Date().getTime();
	var dateinwords = new Date().getDate()+" "+new Date().getMonth()+" "+new Date().getUTCFullYear();
	res.status(200).json({ dummy:"result",message: currTime,stringVal:dateinwords });
});


app.use('/api', router);
